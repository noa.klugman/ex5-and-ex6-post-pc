package exercise.android.reemh.todo_items;

import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;

public class ToDoListActivity extends AppCompatActivity {
    public LocalDataBase dataBase = null;
    private Boolean textIsEmpty = true;
    private Context context = this;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if(dataBase == null){
            dataBase = TodoApplication.getInstance().getDataBase();
        }
        FloatingActionButton createTodoButton = findViewById(R.id.buttonCreateTodoItem);
        EditText textToEdit = findViewById(R.id.editTextInsertTask);
        RecyclerView todo_recycler = findViewById(R.id.recyclerTodoItemsList);
        TodoItemAdapter adapter = new TodoItemAdapter();
        adapter = adapter.settt();

        textToEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                textIsEmpty = false;
            }
        });

        TodoItemAdapter finalAdapter = adapter;
        createTodoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!textIsEmpty){
                    String newDescription = textToEdit.toString();
                    dataBase.addNewInProgressItem(newDescription);
                    finalAdapter.addTodoItem(newDescription);
                    todo_recycler.setAdapter(finalAdapter);
                    todo_recycler.setLayoutManager(new LinearLayoutManager(context, RecyclerView.VERTICAL, false /*reverseLayout*/));
                    textToEdit.setText("");
                }
            }
        });



        dataBase.assignmentsLiveDataPublic.observe(this, new Observer<List<TodoItem>>() {
            @Override
            public void onChanged(List<TodoItem> todoItems) {

            }
        });
    }
}
