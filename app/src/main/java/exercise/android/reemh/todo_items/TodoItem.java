package exercise.android.reemh.todo_items;

public class TodoItem {
    private final String description;
    private final Boolean isDone;
    private final int index;

    TodoItem(String description, Boolean isDone, int index){
        this.description = description;
        this.index = index;
        this.isDone = isDone;
    }

    public String getDescription(){
        return description;
    }

    public Boolean getIsDone() {
        return isDone;
    }

    public int getIndex() {
        return index;
    }
}
