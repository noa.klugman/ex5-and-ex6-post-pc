package exercise.android.reemh.todo_items

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton


class MainActivity : AppCompatActivity() {
    var dataBase: TodoItemsDataBase? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        if (dataBase == null) {
            dataBase = TodoItemsDataBaseImpl()
        }

        // TODO: implement the specs as defined below
        //    (find all UI components, hook them up, connect everything you need)

        val createTodoButton : FloatingActionButton = findViewById(R.id.buttonCreateTodoItem)
        val textToEdit :EditText = findViewById(R.id.editTextInsertTask)
        val todo_recycler: RecyclerView = findViewById(R.id.recyclerTodoItemsList)
        var textIsEmpty : Boolean = true;

        val adapter = TodoItemAdapter()
        adapter.onDonePicClickCallback = {item: TodoItem ->
            dataBase?.markItemInProgress(item)
        }
        adapter.onInprocessPicClickCallback = {item: TodoItem ->
            dataBase?.markItemDone(item)
        }
        adapter.onDeleteClickCallback = {item: TodoItem ->
            dataBase?.deleteItem(item)
        }

        // set listener on the input written by the keyboard to the edit-text
        textToEdit.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
            override fun afterTextChanged(s: Editable) {
                // text did change
                textIsEmpty = false;
            }
        })

        createTodoButton.setOnClickListener{
            if(!textIsEmpty){
                val newDescription: String = textToEdit.text.toString()
                dataBase!!.addNewInProgressItem(newDescription)
                adapter.addTodoItem(newDescription)
                todo_recycler.adapter = adapter
                todo_recycler.layoutManager =
                        LinearLayoutManager(this, RecyclerView.VERTICAL, false /*reverseLayout*/)
                textToEdit.setText("")
            }
        }
    }
}