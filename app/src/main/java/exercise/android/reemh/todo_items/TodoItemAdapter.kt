package exercise.android.reemh.todo_items

import android.content.Context
import android.content.Intent
import android.graphics.Paint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

class TodoItemAdapter: RecyclerView.Adapter<TodoItemHolder>() {
    val itemsDataBase: LocalDataBase = TodoApplication.getInstance().getDataBase();
    public var onDonePicClickCallback: ((TodoItem) -> Unit)? = null
    public var onInprocessPicClickCallback: ((TodoItem) -> Unit)? = null
    public var onDeleteClickCallback: ((TodoItem) -> Unit)? = null

    val adapter = TodoItemAdapter()
    fun settt(): TodoItemAdapter{
        adapter.onDonePicClickCallback = { item: TodoItem ->
            itemsDataBase?.markItemInProgress(item)
        }
        adapter.onInprocessPicClickCallback = { item: TodoItem ->
            itemsDataBase?.markItemDone(item)
        }
        adapter.onDeleteClickCallback = { item: TodoItem ->
            itemsDataBase?.deleteItem(item)
        }
        return adapter;
    }

    override fun getItemCount(): Int {
        return itemsDataBase.currentItems.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TodoItemHolder {
        val context = parent.context
        val view = LayoutInflater.from(context)
                .inflate(R.layout.row_todo_item, parent, false)

        return TodoItemHolder(view)
    }

    override fun onBindViewHolder(holder: TodoItemHolder, position: Int) {
        val todoItem = itemsDataBase.getTodoItem(position)
        holder.descriptionText.setText(todoItem.description)
        if(todoItem.isDone){
            holder.donePic.bringToFront();
        }
        else{
            holder.inprocessPic.bringToFront();
        }
        holder.donePic.setOnClickListener{
            val callback = onDonePicClickCallback ?: return@setOnClickListener
            callback(todoItem)
            holder.inprocessPic.bringToFront();
        }
        holder.inprocessPic.setOnClickListener{
            val callback = onInprocessPicClickCallback ?: return@setOnClickListener
            callback(todoItem)
            holder.donePic.bringToFront();
            holder.descriptionText.apply {
                paintFlags = paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
            }
        }
        holder.deleteButton.setOnClickListener(){
            val callback = onDeleteClickCallback ?: return@setOnClickListener
            callback(todoItem)
            itemsDataBase.deleteItem(todoItem)
        }
    }

    fun addTodoItem(newDescription: String){
        itemsDataBase.addNewInProgressItem(newDescription)
        notifyDataSetChanged()
    }
}