package exercise.android.reemh.todo_items;

import android.content.Context;
import android.content.SharedPreferences;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class LocalDataBase {
    private final ArrayList<TodoItem> assignments = new ArrayList<>();
    private final Context context;
    private final SharedPreferences sp;
    private final MutableLiveData<List<TodoItem>> assignmentsLiveDataMutable = new MutableLiveData<>();
    public final LiveData<List<TodoItem>> assignmentsLiveDataPublic = assignmentsLiveDataMutable;
    int numOfToDo = 0;

    public LocalDataBase(Context context) {
        this.context = context;
        this.sp = context.getSharedPreferences("local_db_todo", Context.MODE_PRIVATE);
        initializeFromSp();
    }

    private void initializeFromSp(){
        Set<String> keys = sp.getAll().keySet();
        for (String key : keys) {
            String descriptionToRestore = sp.getString(key, null);
            if (descriptionToRestore != null){
                assignments.add(new TodoItem(descriptionToRestore, false, numOfToDo));
                numOfToDo++;
            }
        }
        assignmentsLiveDataMutable.setValue(new ArrayList<>(assignments));
    }

    public List<TodoItem> getCurrentItems() {
        return new ArrayList<TodoItem>(assignments);
    }

    public void addNewInProgressItem(String description) {
        TodoItem newItem = new TodoItem(description, false, numOfToDo);
        assignments.add(newItem);
        assignmentsLiveDataMutable.setValue(new ArrayList<>(assignments));
        SharedPreferences.Editor editor = sp.edit();
        editor.putString("" + numOfToDo, description);
        numOfToDo++;
        editor.apply();
    }

    public void markItemDone(TodoItem itemToChange) {
        TodoItem newItem = null;
        for (TodoItem item : assignments) {
            if(item.getDescription() == itemToChange.getDescription()){
                newItem = new TodoItem(itemToChange.getDescription(), true, item.getIndex());
                assignments.remove(item);
                assignments.add(newItem);
                assignmentsLiveDataMutable.setValue(new ArrayList<>(assignments));
                break;
            }
        }
        if(newItem == null) return;
        SharedPreferences.Editor editor = sp.edit();
        editor.putString("" + newItem.getIndex(), newItem.getDescription());
        editor.apply();
    }

    public void markItemInProgress(TodoItem itemToChange) {
        TodoItem newItem = null;
        for (TodoItem item : assignments) {
            if(item.getDescription() == itemToChange.getDescription()){
                newItem = new TodoItem(itemToChange.getDescription(), false, item.getIndex());
                assignments.remove(item);
                assignments.add(newItem);
                assignmentsLiveDataMutable.setValue(new ArrayList<>(assignments));
                break;
            }
        }
        if(newItem == null) return;
        SharedPreferences.Editor editor = sp.edit();
        editor.putString("" + newItem.getIndex(), newItem.getDescription());
        editor.apply();
    }

    public void deleteItem(TodoItem itemToDelete) {
        Boolean found = false;
        String key = "";
        for (TodoItem item : assignments) {
            if(item.getDescription() == itemToDelete.getDescription()){
                key = "" + item.getIndex();
                assignments.remove(item);
                assignmentsLiveDataMutable.setValue(new ArrayList<>(assignments));
                found = true;
                break;
            }
        }
        if(!found) return;
        SharedPreferences.Editor editor = sp.edit();
        editor.remove(key);
        editor.apply();
    }

    public TodoItem getTodoItem(int position) {
        return assignments.get(position);
    }

}
