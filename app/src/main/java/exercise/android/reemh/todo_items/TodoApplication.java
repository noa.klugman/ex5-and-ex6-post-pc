package exercise.android.reemh.todo_items;

import android.app.Application;

public class TodoApplication extends Application {
    private LocalDataBase database;

    public LocalDataBase getDataBase(){
        return database;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        database = new LocalDataBase(this);
    }

    public static TodoApplication instance = null;
    public static TodoApplication getInstance() {
        return instance;
    }
}