package exercise.android.reemh.todo_items

// TODO: implement!
class TodoItemsDataBaseImpl : TodoItemsDataBase {
    val list_items: MutableList<TodoItem> = ArrayList<TodoItem>()

    override fun getCurrentItems(): List<TodoItem> {
        return list_items
    }

    override fun addNewInProgressItem(description: String) {
        //list_items.add(TodoItem(description, false))
    }

    override fun markItemDone(item: TodoItem) {
        val itemToChange = list_items.find { item.description == it.description } ?: return
       // itemToChange.isDone = true;
    }

    override fun markItemInProgress(item: TodoItem) {
        val itemToChange = list_items.find { item.description == it.description } ?: return
  //      itemToChange.isDone = false;
    }

    override fun deleteItem(item: TodoItem) {
        list_items.remove(item)
    }

    override fun getTodoItem(position: Int): TodoItem {
        return list_items[position]
    }

}