package exercise.android.reemh.todo_items

import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.constraintlayout.utils.widget.ImageFilterView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView

class TodoItemHolder(view: View) : RecyclerView.ViewHolder(view){
    val descriptionText: TextView = view.findViewById(R.id.description_text)
    val donePic : ImageFilterView = view.findViewById(R.id.done_pic)
    val inprocessPic : ImageFilterView = view.findViewById(R.id.inprocess_pic)
    val deleteButton : Button = view.findViewById(R.id.delete_button)
    val todo_item : ConstraintLayout = view.findViewById(R.id.todo_item);

}